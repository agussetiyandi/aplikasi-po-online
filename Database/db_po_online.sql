-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17 Feb 2019 pada 14.58
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_po_online`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `po_management`
--

CREATE TABLE `po_management` (
  `id_po_management` int(11) NOT NULL,
  `po_management_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `po_management`
--

INSERT INTO `po_management` (`id_po_management`, `po_management_name`) VALUES
(1, 'PO-1'),
(2, 'PO-2'),
(3, 'PO-3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `id_product` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_category` varchar(100) NOT NULL,
  `capital_price_usd` int(11) NOT NULL,
  `additional_fee` int(11) NOT NULL,
  `price_rupiah` int(11) NOT NULL,
  `detail_price_rupiah` int(11) NOT NULL,
  `size` varchar(50) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `images` blob,
  `stock` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id_product`, `product_name`, `product_category`, `capital_price_usd`, `additional_fee`, `price_rupiah`, `detail_price_rupiah`, `size`, `color`, `images`, `stock`) VALUES
(1, 'KOL', 'haha', 31, 33, 32, 0, '5', '3', '', 0),
(2, 'Putra', 'haha', 10000, 10000, 10000, 10000, '21', 'red', '', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_category`
--

CREATE TABLE `product_category` (
  `id_category` int(255) NOT NULL,
  `product_category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_category`
--

INSERT INTO `product_category` (`id_category`, `product_category`) VALUES
(1, 'haha');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_color`
--

CREATE TABLE `product_color` (
  `id_product` int(11) NOT NULL,
  `color` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_color`
--

INSERT INTO `product_color` (`id_product`, `color`) VALUES
(1, 'Red'),
(2, 'Blue'),
(2, 'Yellow');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_size`
--

CREATE TABLE `product_size` (
  `id_product` int(11) NOT NULL,
  `size` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_size`
--

INSERT INTO `product_size` (`id_product`, `size`) VALUES
(1, '42'),
(1, '44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id_bon` int(255) NOT NULL,
  `po_group` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `total_payment` int(255) NOT NULL DEFAULT '0',
  `current_payment` int(255) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_order`
--

INSERT INTO `purchase_order` (`id_bon`, `po_group`, `name`, `email`, `phone`, `contact`, `address`, `bank`, `total_payment`, `current_payment`, `date`, `note`) VALUES
(2, 'PO-2', 'KOLL', 'KOLL2@gmail.com', '08123456791', '[WhatsApp];KOLL', 'KOLL', 'BCA', 6531, 3750, '2018-12-28 08:16:58', 'KOLL'),
(3, 'PO-2', 'Habib', 'habibsumedang@gmail.com', '08123456791', '[Line @];habib_syuhada', 'Batam', 'Mandiri', 200350, 9145000, '2018-12-31 08:42:42', 'Kirim'),
(5, 'PO-2', 'saya', 'saya@gmail.com', '081111111111', '[Line @];saya', 'Disini', 'BCA', 0, 0, '2018-12-31 19:33:52', 'Tolong'),
(6, 'PO-2', 'asd', 'habibsumedang@gmail.com', 'asd', '[Line @];asd', 'asd', 'BCA', 0, 0, '2019-01-02 11:55:15', 'asd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_order_group`
--

CREATE TABLE `purchase_order_group` (
  `id` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `kurs` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_order_group`
--

INSERT INTO `purchase_order_group` (`id`, `note`, `kurs`, `date_created`) VALUES
('PO-1', 'Test Aja', 0, '2018-12-31 18:25:03'),
('PO-2', 'Test Yang Kedua', 1000, '2018-12-31 19:17:16'),
('PO-3', 'Pengen Test Lagi', 15000, '2018-12-31 19:17:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_order_payment`
--

CREATE TABLE `purchase_order_payment` (
  `id` int(255) NOT NULL,
  `id_bon` int(255) NOT NULL,
  `payment` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_order_payment`
--

INSERT INTO `purchase_order_payment` (`id`, `id_bon`, `payment`) VALUES
(1, 2, 3250),
(3, 3, 110000),
(4, 2, 500),
(5, 3, 9035000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_order_product`
--

CREATE TABLE `purchase_order_product` (
  `id` int(11) NOT NULL,
  `id_bon` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `flag2_qty` varchar(255) DEFAULT NULL,
  `action_qty` int(11) DEFAULT NULL,
  `flag2_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_order_product`
--

INSERT INTO `purchase_order_product` (`id`, `id_bon`, `id_product`, `name`, `qty`, `color`, `size`, `price`, `note`, `flag`, `flag2_qty`, `action_qty`, `flag2_date`) VALUES
(3, 2, 2, 'Putra', 1, 'Blue', 'N/A', 1500, '1', 2, '1', 0, '2019-01-03 15:35:36'),
(4, 2, 1, 'KOL', 1, 'Red', '42', 1000, '1', 1, NULL, NULL, NULL),
(5, 3, 1, 'KOL', 10, 'Red', '42', 350, '', 2, '4', 5, '2019-02-01 05:24:13'),
(6, 3, 2, 'Putra', 15, 'Blue', ' N/A', 200000, '', 2, '15', 0, '2019-02-01 05:14:03'),
(7, 2, 1, 'KOL', 1, 'Red', '42', 31, '', 2, '0', 0, '2019-02-01 05:15:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_order_product_etc`
--

CREATE TABLE `purchase_order_product_etc` (
  `id` int(255) NOT NULL,
  `id_bon` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` int(255) NOT NULL,
  `price` int(255) NOT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `purchase_order_product_etc`
--

INSERT INTO `purchase_order_product_etc` (`id`, `id_bon`, `name`, `quantity`, `price`, `note`) VALUES
(2, 2, '1', 1, 1000, ''),
(3, 2, '123', 1, 3000, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase_order_resi`
--

CREATE TABLE `purchase_order_resi` (
  `id` int(255) NOT NULL,
  `id_bon` int(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `resi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase_order_resi`
--

INSERT INTO `purchase_order_resi` (`id`, `id_bon`, `sender`, `resi`) VALUES
(1, 1, 'JNE', '123'),
(2, 1, 'TIKI', '321');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `password`, `level`) VALUES
(2, 'Admin', 'admin', '202cb962ac59075b964b07152d234b70', 'admin'),
(3, 'Karyawan', 'karyawan', '202cb962ac59075b964b07152d234b70', 'karyawan'),
(4, 'Manager', 'manager', '202cb962ac59075b964b07152d234b70', 'manager');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_flagging`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_flagging` (
`id` int(11)
,`id_bon` int(11)
,`po_group` varchar(255)
,`name_costumer` varchar(255)
,`id_product` int(11)
,`name` varchar(255)
,`images` blob
,`qty` int(11)
,`color` varchar(255)
,`size` varchar(255)
,`price` int(11)
,`note` varchar(255)
,`flag` int(11)
,`total_book` double
,`stock` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_flagging2`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_flagging2` (
`id` int(11)
,`id_bon` int(11)
,`id_product` int(11)
,`po_group` varchar(255)
,`name_costumer` varchar(255)
,`name` varchar(255)
,`qty` int(11)
,`color` varchar(255)
,`size` varchar(255)
,`price` int(11)
,`note` varchar(255)
,`flag` int(11)
,`flag2_qty` varchar(255)
,`flag2_date` datetime
,`total_book` double
,`stock` int(11)
,`current_payment` int(255)
,`total_payment` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_total_qty_flagging2`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_total_qty_flagging2` (
`id_product` int(11)
,`name` varchar(255)
,`total_book` double
,`stock` int(11)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_flagging`
--
DROP TABLE IF EXISTS `v_flagging`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_flagging`  AS  select `a`.`id` AS `id`,`a`.`id_bon` AS `id_bon`,`c`.`po_group` AS `po_group`,`c`.`name` AS `name_costumer`,`a`.`id_product` AS `id_product`,`a`.`name` AS `name`,`d`.`images` AS `images`,`a`.`qty` AS `qty`,`a`.`color` AS `color`,`a`.`size` AS `size`,`a`.`price` AS `price`,`a`.`note` AS `note`,`a`.`flag` AS `flag`,`b`.`total_book` AS `total_book`,`b`.`stock` AS `stock` from (((`purchase_order_product` `a` join `v_total_qty_flagging2` `b`) join `purchase_order` `c`) join `product` `d` on(((`a`.`id_product` = `b`.`id_product`) and (`a`.`id_bon` = `c`.`id_bon`) and (`a`.`id_product` = `d`.`id_product`)))) where ((`a`.`flag` not in (2,3)) and `a`.`id_bon` in (select `purchase_order`.`id_bon` from `purchase_order` where ((`purchase_order`.`total_payment` / 2) <= `purchase_order`.`current_payment`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_flagging2`
--
DROP TABLE IF EXISTS `v_flagging2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_flagging2`  AS  select `a`.`id` AS `id`,`a`.`id_bon` AS `id_bon`,`a`.`id_product` AS `id_product`,`c`.`po_group` AS `po_group`,`c`.`name` AS `name_costumer`,`a`.`name` AS `name`,`a`.`qty` AS `qty`,`a`.`color` AS `color`,`a`.`size` AS `size`,`a`.`price` AS `price`,`a`.`note` AS `note`,`a`.`flag` AS `flag`,`a`.`flag2_qty` AS `flag2_qty`,`a`.`flag2_date` AS `flag2_date`,`b`.`total_book` AS `total_book`,`b`.`stock` AS `stock`,`c`.`current_payment` AS `current_payment`,`c`.`total_payment` AS `total_payment` from ((`purchase_order_product` `a` join `v_total_qty_flagging2` `b` on((`a`.`id_product` = `b`.`id_product`))) join `purchase_order` `c` on((`a`.`id_bon` = `c`.`id_bon`))) where (`a`.`flag` = 2) order by `a`.`id_bon` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_total_qty_flagging2`
--
DROP TABLE IF EXISTS `v_total_qty_flagging2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_total_qty_flagging2`  AS  select `a`.`id_product` AS `id_product`,`a`.`name` AS `name`,sum((case when (`a`.`flag` = 2) then `a`.`flag2_qty` else 0 end)) AS `total_book`,`b`.`stock` AS `stock` from (`purchase_order_product` `a` join `product` `b` on((`a`.`id_product` = `b`.`id_product`))) group by `a`.`id_product`,`a`.`name`,`b`.`stock` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `po_management`
--
ALTER TABLE `po_management`
  ADD PRIMARY KEY (`id_po_management`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id_bon`);

--
-- Indexes for table `purchase_order_group`
--
ALTER TABLE `purchase_order_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_payment`
--
ALTER TABLE `purchase_order_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_product`
--
ALTER TABLE `purchase_order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_product_etc`
--
ALTER TABLE `purchase_order_product_etc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_resi`
--
ALTER TABLE `purchase_order_resi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `po_management`
--
ALTER TABLE `po_management`
  MODIFY `id_po_management` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id_category` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id_bon` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `purchase_order_payment`
--
ALTER TABLE `purchase_order_payment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `purchase_order_product`
--
ALTER TABLE `purchase_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `purchase_order_product_etc`
--
ALTER TABLE `purchase_order_product_etc`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `purchase_order_resi`
--
ALTER TABLE `purchase_order_resi`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
