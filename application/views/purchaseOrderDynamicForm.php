<?php foreach($list_product->result() as $product) : ?>
<input name="name" type="hidden" class="form-control" value="<?= $product->product_name ?>">
<?php endforeach; ?>
<div class="form-group">
	<label>Quantity</label>
	<input name="quantity" type="number" class="form-control" placeholder=". . ." required>
</div>
<div class="form-group">
	<label>Color</label>
	<select name="color" class="form-control" required>
		<option value="<?php echo (count($list_size->result()) == 0 ? " N/A" : "" ) ?>">- Select Option -</option>
		<?php foreach($list_color->result() as $row) : ?>
		<?php $color = $row->color; ?>
		<option value="<?php echo $color ?>">
			<?php echo $color ?>
		</option>
		<?php endforeach; ?>
	</select>
</div>
<div class="form-group">
	<label>Size</label>
	<select name="size" class="form-control" required>
		<option value="<?php echo (count($list_size->result()) == 0 ? " N/A" : "" ) ?>">- Select Option -</option>
		<?php foreach($list_size->result() as $row) : ?>
		<option value="<?= $row->size ?>">
			<?= $row->size ?>
		</option>
		<?php endforeach; ?>
	</select>
</div>
<table border="1" width="100%" class="nowrap text-center">
	<!-- <tr style="background-color: #35c487; color: white; font-weight: bold">
		<td colspan="2">Harga Modal EUR</td>
	</tr> -->
	<!-- <tr>
		<td colspan="2">Rp
			<?= number_format($product->capital_price_usd,2,",",".") ?>
		</td>
	</tr> -->
	<tr style="background-color: #35c487; color: white; font-weight: bold">
		<td>Harga Modal [EUR]</td>
		<td>Harga Modal [Rp]</td>
	</tr>
	<tr>
		<td>Rp
			<?= number_format($product->capital_price_usd,2,",",".") ?>
		</td>
		<td>Rp
			<?= number_format($product->price_rupiah,2,",",".") ?>
		</td>
	</tr>
	<!-- <tr style="background-color: #35c487; color: white; font-weight: bold">
		<td>Harga WS RP</td>
		<td>Harga Retail RP</td>
	</tr>
	<tr>
		<td><?= $product->price_rupiah ?></td>
		<td>Comming Soon</td>
	</tr> -->
</table>
<div class="form-group">
	<label>Harga Jual (Disetujui)</label>
	<input name="price" type="number" class="form-control" placeholder=". . ." required>
</div>
<div class="form-group">
	<label>Note</label>
	<textarea name="note" class="form-control" rows="5" placeholder=". . ."></textarea>
</div>
