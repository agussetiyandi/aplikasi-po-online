<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Flagging 2</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Flagging 2</h4>
								</div>
								<div class="col-md-4">
									<select class="form-control" onchange="changegroup(this.value);">
										<option value="All" <?php echo ($groupall == '1' ? 'selected' : '') ?>>All</option>
										<?php foreach($list_po_group->result() as $group) : ?>
										<option value="<?= $group->id ?>" <?php echo ($groupall == $group->id ? 'selected' : '') ?>><?= $group->id ?></option>
										<?php endforeach ;?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">No.</th>
											<th>Bon ID</th>
											<th>Bacth PO</th>
											<th>Costumer</th>
											<th width="25%">Product Name</th>
											<th>Color</th>
											<th>Size</th>
											<th>Total</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$no = 1;
											foreach($list_flag->result() as $row) :
												$qty = $row->qty;
												$total_book = $row->total_book;
												$stock = $row->stock;
												$kurang = (($stock - $total_book) - $qty)*-1;

												$current_payment = $row->current_payment;
												$total_payment = $row->total_payment;
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td><b><a href="<?php echo site_url('PO/purchaseOrderDetail/') ?><?= $row->id_bon ?>"><?= $row->id_bon ?></a></b></td>
											<td><?= $row->po_group ?></td>
											<td><?= $row->name_costumer ?></td>
											<td><?= $row->name ?></td>
											<td><?= $row->color ?></td>
											<td><?= $row->size ?></td>
											<td>
												<?= $row->flag2_qty ?>
												<?php if($row->flag2_qty < $row->qty): ?>
													<br><span style="cursor: pointer" onclick="updateflagqty(<?= $row->id_bon ?>, <?= $row->id ?>, <?= $row->id_product ?>)" class="badge badge-danger">Kurang <?php echo $row->flag2_qty - $row->qty ?></span>
												<?php endif; ?>
											</td>
											<td>
											<?php 
												if($current_payment < ($total_payment/2)):
											?>
												<span class="badge badge-danger">Belum Lunas</span>
											<?php
												elseif($current_payment < $total_payment):
											?>
												<span class="badge badge-warning">DP Lunas</span>
											<?php
												else:
											?>
												<span class="badge badge-success">Lunas</span>
											<?php
												endif;
											?>
											</td>
											<td>
												<a href="<?php echo site_url('Flagging2/flagging2Cancel/'.$row->id) ?>" onclick="return confirm('Are you sure to cancel this product?')" title="Back to Flagging" type="button" class="btn btn-danger">
													<i class="fa fa-reply"></i>
												</a>
												<!-- <a href="<?php echo site_url('Flagging2/flagging2Send/'.$row->id.'/'.$row->id_product.'/'.$row->flag2_qty) ?>" onclick="return confirm('Are you sure to send <?= $row->flag2_qty ?> <?= $row->name ?> ?')" title="Delete" type="button" class="btn btn-success">
													<i class="fa fa-check"></i>
												</a> -->
											</td>
										</tr>
										<?php
											endforeach;
										?>
									</tbody>
								</table>
							</div>
						</div><!-- end row -->
					</div><!-- container-fluid -->
				</div><!-- content -->
				<?php $this->load->view("_partials/footer.php") ?>
			</div><!-- ============================================================== -->
			<!-- End Right content here -->
			<!-- ============================================================== -->
		</div>
		<!-- END wrapper -->
		<div id="updateflagqtyModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content" id="dynamicform">
					
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>

		<!-- jQuery  -->

		<?php $this->load->view("_partials/js.php") ?>
		<script type="text/javascript">
			$("#datatable").DataTable();

			function updateflagqty(id_bon, id, id_product){
				console.log(id_bon+' '+id);
				var link = "<?php echo $groupall; ?>";
				console.log(link);
				$.ajax({
					url: "<?php echo site_url('Flagging2/flagging2UpdateQtyModal/') ?>"+id_product+"/"+id+"/"+link,
					success: function(data){
						$("#dynamicform").html(data);
					}
				})
				// $("#deletePOId").html(id_bon);
				// $("#linkdelete").prop("href", "<?php echo site_url('PO/purchaseOrderDelete/') ?>" + id_bon);
				$("#updateflagqtyModal").modal("show");
			}
			function changegroup(id_group){
				if(id_group == 'All'){
					window.location.href = "<?php echo site_url('Flagging2/flagging2ListAll') ?>";
				}
				else{
					window.location.href = "<?php echo site_url('Flagging2/flagging2ListGroup/') ?>"+id_group;
				}
			}
		</script>

</body>

</html>
