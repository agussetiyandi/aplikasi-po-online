<?php foreach($flag_data->result() as $flag) : ?>
<?php foreach($product_data->result() as $row) : ?>
<div class="modal-header">
	<h5 class="modal-title mt-0" id="myModalLabel"><?= $row->product_name ?></h5>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<form method="POST" action="<?php echo base_url('Flagging2/flagging2UpdateQty'); ?>">
<input name="id" class="form-control" type="hidden" value="<?= $flag->id ?>" readonly>
<input name="link" class="form-control" type="hidden" value="<?= $link ?>" readonly>
<div class="modal-body">
	<div class="form-group row">
		<label class="col-sm-5 col-form-label text-right">Kuantitas Kurang</label>
		<div class="col-sm-7">
			<input name="total_stock_available" class="form-control" type="text" value="<?php echo $flag->qty - $flag->flag2_qty ?>" readonly>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-5 col-form-label text-right">Tambah Kuantitas</label>
		<div class="col-sm-7">
			<input name="total_order" class="form-control" type="number">
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
	<button type="submit" class="btn btn-warning waves-effect">Update</button>
</div>
</form>
<?php endforeach; ?>
<?php endforeach; ?>