<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Setting</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="card">
					<div class="card-header">
						<h5 class="page-title mb-0"><button data-target="#AddResi" data-toggle="modal" class="btn btn-success pull-right" onclick="addpogroup();">Add PO Group</button>Setting</h5>

					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">No.</th>
											<th>PO Group ID</th>
											<th>Note</th>
											<th>Kurs 1 EUR</th>
											<th>Date Created</th>
											<th>Action</th>
										</tr>
									</thead>
										<?php
											$no = 1;
											foreach($list_po_group->result() as $row) :
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $row->id ?></td>
											<td><?= $row->note ?></td>
											<td><?php echo "Rp".number_format($row->kurs,2,",",".") ?></td>
											<td><?= $row->date_created ?></td>
											<td>
												<div class="btn-group">
													<button onclick="deleteConfirm('<?= $row->id ?>');" title="Delete" type="button" class="btn btn-danger">
														<i class="fa fa-trash"></i>
													</button>
												</div>
											</td>
										</tr>
										<?php
											endforeach;
										?>	
									<tbody>
									</tbody>
								</table>
							</div>
						</div><!-- end row -->
					</div><!-- container-fluid -->
				</div><!-- content -->
				<?php $this->load->view("_partials/footer.php") ?>
			</div><!-- ============================================================== -->
			<!-- End Right content here -->
			<!-- ============================================================== -->
		</div>
	</div><!-- END wrapper -->

	<div id="AddResi" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Add New PO Group</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<form method="POST" action="<?php echo site_url('Setting/settingAddPOGroup') ?>">
					<div class="modal-body">
						<div class="form-group">
							<label>PO Group ID</label> 
							<input name="id" type="text" class="form-control" placeholder="PO ID" required readonly value="PO-<?php echo $num_po_group+1 ?>">
						</div>
						<div class="form-group">
							<label>Kurs</label> 
							<input name="kurs" type="number" class="form-control" placeholder="Kurs 1 EUR = Rp 0" required>
						</div>
						<div class="form-group">
							<label>Note</label> 
							<textarea name="note" class="form-control" rows="5" placeholder=". . ."></textarea>
						</div>
					</div>
					<div class="modal-footer">
					<div class="form-group mb-0">
						<div>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
							<button type="submit" class="btn btn-success waves-effect"><i class="fa fa-plus"></i> Add</button> 
						</div>
					</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<!-- jQuery  -->

	<div id="deleteConfirmModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Delete Confirm</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<h6>Are you sure to delete <b><span id='deleteCatId'>asd</span></b></h6>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
					<a id="linkdelete" href="" class="btn btn-danger waves-effect waves-light">Delete</a>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>
  <script type="text/javascript">
		$("#datatable").DataTable();
		function deleteConfirm(id_cat) {
			console.log('buka modal');
			$("#deleteCatId").html(id_cat);
			$("#linkdelete").prop("href", "<?php echo site_url('Setting/catDeletePoGroup/') ?>"+id_cat);
			$("#deleteConfirmModal").modal("show");
		}
  </script>

</body>

</html>
