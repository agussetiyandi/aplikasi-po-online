<?php

ini_set("display_errors", 1);


// Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = base_url('assets/images/pdflogo.png'); // *** Very IMP: make sure this image is available on given path on your server
        $this->Image($image_file,15,6,25);
        // Set font
        $this->SetFont('helvetica', 'BU', 20);
    
        // Line break
        $this->Ln();        
        $this->Cell(294, 30, 'A2 Luxury', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->Ln(9);        
        $this->SetFont('helvetica', 'C', 12);
        $this->Cell(300, 0, 'Instagram : a2.luxury', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->Ln(6);        
        $this->Cell(291, 0, 'Line : a2_luxury', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        // We need to adjust the x and y positions of this text ... first two parameters
        
    }

    // Page footer
    public function Footer() {
        /*// Position at 25 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        
        $this->Cell(0, 0, 'Product Company - ABC test company, Phone : +91 1122 3344 55, TIC : TESTTEST', 0, 0, 'C');
        $this->Ln();
        $this->Cell(0,0,'www.clientsite.com - T : +91 1 123 45 64 - E : info@clientsite.com', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');*/
    }
    
}

///////////////// End of class ////////////////////////////

//////////////////////////////////
//
//
// Create new PDF document
//
//
//////////////////////////////////
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nilesh Zemse');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');
$pdf->SetKeywords('PDF, Invoice');


// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
// $pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 15); 
// *** Very IMP: Please use times font, so that if you send this pdf file in gmail as attachment and if user
//opens it in google document, then all the text within the pdf would be visible properly.

// add a page
$pdf->AddPage();
$no = 1;
foreach($data->result() as $row) :
// create some HTML content
$now = date("d M Y", strtotime($row->date));
$company_name = 'ABC test company';

$user_name = $row->name;
$invoice_ref_id = 'A2000'.$row->id_bon;
$telp = $row->phone;
$alamat = $row->address;
$cur_pay = $row->current_payment;

endforeach;

// *** IMP: The value of $html and $html_terms can come from db
// But, If these values contain, other language special characters, then
// PDF is not getting generated. in that case should find such invalid charactes and 
// make use of its htmlentity substitute 
// for ex. If copyright is invalid character then use &copy; in html content


// $html on page 1 of PDF and $html_terms are on page 2 of PDF


$html = '';
$html .= '<table cellpadding="3">
            <tr>
                <td colspan="6" align="center"><u><h1>Invoice</b></h1></td>
            </tr>
            <tr>
                <td colspan="6" align="right"><u>{now}</u></td>
            </tr>
            <tr>
                <td colspan="4">
                    <table style="background-color: #ffffff; border:5px black solid;">
                        <tr>
                            <td colspan="4">Shipping Address</td>
                        </tr>
                        <tr>
                            <td>Penerima</td>
                            <td colspan="3">: {user_name}</td>
                        </tr>
                        <tr>
                            <td>Telepon</td>
                            <td colspan="3">: {telp}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td colspan="3">: {alamat}</td>
                        </tr>
                        <tr>
                            <td>Pengirim:</td>
                            <td colspan="3">: A2 Romi - +62 852 73111998</td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
         </table>';

$html .= '<br><br>
          <table border="1" cellpadding="5">
            <tr>
                <td colspan="7">Invoice # {invoice_ref_id}</td>            
            </tr>
            <tr>
                <td colspan="4"><b>Product</b></td>
                <td><b>Quantity</b></td>
                <td align="right" colspan="2"><b>Amount (Rp.)</b></td>
            </tr>';
$sum = 0;  
foreach($list_product->result() as $row) :

$sum = $sum + $row->price;
$html .= '<tr>
            <td colspan="4">'.$row->name.' - '.$row->color.' - '.$row->size.'</td>
            <td>'.number_format($row->qty).'</td>
            <td align="right" colspan="2">Rp. '.number_format($row->price).'</td>
          </tr>';
endforeach;
$html .= '<tr>
            <td colspan="5" align="right"><b>Total: </b></td>
            <td colspan="2" align="right"><b>Rp. '.number_format($sum).'</b></td>
        </tr>
        <tr>
            <td colspan="5" align="right"><b>Current Payment: </b></td>
            <td colspan="2" align="right"><b>Rp. '.number_format($cur_pay).'</b></td>
        </tr>
        <tr>
            <td colspan="5" align="right"><b>Remaining Payment: </b></td>
            <td colspan="2" align="right"><b>Rp. '.number_format($sum - $cur_pay).'</b></td>
        </tr>
        </table>';

$html = str_replace('{now}',$now, $html);
$html = str_replace('{company_name}',$company_name, $html);
$html = str_replace('{user_name}',$user_name, $html);
$html = str_replace('{invoice_ref_id}',$invoice_ref_id, $html);
$html = str_replace('{telp}',$telp, $html);
$html = str_replace('{alamat}',$alamat, $html);


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// add a page
//Close and output PDF document
$pdf_file_name = 'Bon_Report.pdf';
$pdf->Output($pdf_file_name, 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
?>