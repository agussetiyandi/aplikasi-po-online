<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Dashboard</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-xl-3 col-md-6">
						<div class="card mini-stats">
							<div class="p-3 mini-stats-content">
								<div class="mb-4">
									<div class="float-right text-right"><span class="badge badge-light text-info mt-2 mb-2">+ 11%</span>
										<p class="text-white-50">From previous period</p>
									</div><span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}'
									 data-width="54" data-height="54">5/8</span>
								</div>
							</div>
							<div class="ml-3 mr-3">
								<div class="bg-white p-3 mini-stats-desc rounded">
									<h5 class="float-right mt-0">1758</h5>
									<h6 class="mt-0 mb-3">Orders</h6>
									<p class="text-muted mb-0">Sed ut perspiciatis unde iste</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-md-6">
						<div class="card mini-stats">
							<div class="p-3 mini-stats-content">
								<div class="mb-4">
									<div class="float-right text-right"><span class="badge badge-light text-danger mt-2 mb-2">- 27%</span>
										<p class="text-white-50">From previous period</p>
									</div><span class="peity-donut" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"], "innerRadius": 18, "radius": 32 }'
									 data-width="54" data-height="54">2/5</span>
								</div>
							</div>
							<div class="ml-3 mr-3">
								<div class="bg-white p-3 mini-stats-desc rounded">
									<h5 class="float-right mt-0">48259</h5>
									<h6 class="mt-0 mb-3">Revenue</h6>
									<p class="text-muted mb-0">Sed ut perspiciatis unde iste</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-md-6">
						<div class="card mini-stats">
							<div class="p-3 mini-stats-content">
								<div class="mb-4">
									<div class="float-right text-right"><span class="badge badge-light text-primary mt-2 mb-2">0%</span>
										<p class="text-white-50">From previous period</p>
									</div><span class="peity-pie" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"]}'
									 data-width="54" data-height="54">3/8</span>
								</div>
							</div>
							<div class="ml-3 mr-3">
								<div class="bg-white p-3 mini-stats-desc rounded">
									<h5 class="float-right mt-0">$17.5</h5>
									<h6 class="mt-0 mb-3">Average Price</h6>
									<p class="text-muted mb-0">Sed ut perspiciatis unde iste</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-md-6">
						<div class="card mini-stats">
							<div class="p-3 mini-stats-content">
								<div class="mb-4">
									<div class="float-right text-right"><span class="badge badge-light text-info mt-2 mb-2">- 89%</span>
										<p class="text-white-50">From previous period</p>
									</div><span class="peity-donut" data-peity='{ "fill": ["rgba(255, 255, 255, 0.8)", "rgba(255, 255, 255, 0.2)"], "innerRadius": 18, "radius": 32 }'
									 data-width="54" data-height="54">3/5</span>
								</div>
							</div>
							<div class="ml-3 mr-3">
								<div class="bg-white p-3 mini-stats-desc rounded">
									<h5 class="float-right mt-0">2048</h5>
									<h6 class="mt-0 mb-3">Product Sold</h6>
									<p class="text-muted mb-0">Sed ut perspiciatis unde iste</p>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<h5>#10 Top Spenders</h5>
							</div>
							<div class="card-body">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Rank</th>
											<th>Name</th>
											<th>Total Item</th>
											<th>Total Spending</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<h5>Income This PO</h5>
							</div>
							<div class="card-body">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Rank</th>
											<th>Name</th>
											<th>Total Item</th>
											<th>Total Spending</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<h5>Top Selling Item</h5>
							</div>
							<div class="card-body">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Rank</th>
											<th>Name</th>
											<th>Total Item</th>
											<th>Total Spending</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
										<tr>
											<td>1</td>
											<td>Putra</td>
											<td>14</td>
											<td>Rp. 1000.00</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>

</body>

</html>
