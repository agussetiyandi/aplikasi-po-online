<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Purchase Order Batch</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Purchase Order</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Purchase Order</a></li>
										<li class="breadcrumb-item active" aria-current="page">Add Batch</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-xl-5">
						<div class="card">
							<form method="POST" action="<?php echo site_url('Product/catInsert') ?>">
								<div class="card-body">
									<h4 class="mt-0 header-title mb-5">Add Batch Purchase Order</h4>
									<div class="form-group">
										<div><input parsley-type="text" type="text" name="category" class="form-control" required placeholder="Batch Purchase Order"></div>
									</div>
									<div class="form-group mb-0">
										<div><button type="submit" class="btn btn-pink waves-effect waves-light">Add Batch</button></div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-xl-7">
						<div class="card">
							<div class="card-body">
								<h4 class="mt-0 header-title mb-5">Purchase Order Batch</h4>
                <table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width=10%>No.</th>
											<th>Purchase Order Batch</th>
                      <th width=20%>Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->

	<div id="deleteConfirmModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Delete Confirm</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<h6>Are you sure to delete PO.<b><span id='deleteCatId'>asd</span></b></h6>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
					<a id="linkdelete" href="" class="btn btn-danger waves-effect waves-light">Delete</a>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>
  <script type="text/javascript">
		$("#datatable").DataTable();
		function deleteConfirm(id_cat) {
			console.log('buka modal');
			$("#deleteCatId").html(id_cat);
			$("#linkdelete").prop("href", "<?php echo site_url('Product/catDelete/') ?>"+id_cat);
			$("#deleteConfirmModal").modal("show");
		}
  </script>
  
</body>

</html>
