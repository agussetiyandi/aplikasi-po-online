<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Flagging</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Flagging</h4>
								</div>
								<div class="col-md-4">
									<select class="form-control" onchange="changegroup(this.value);">
										<option value="All" <?php echo ($groupall == '1' ? 'selected' : '') ?>>All</option>
										<?php foreach($list_po_group->result() as $group) : ?>
										<option value="<?= $group->id ?>" <?php echo ($groupall == $group->id ? 'selected' : '') ?>><?= $group->id ?></option>
										<?php endforeach ;?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="card">
					<div class="card-body" style="overflow-x: auto">
						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">No.</th>
											<th>Bon ID</th>
											<th>Bacth PO</th>
											<th>Costumer</th>
											<th width="25%">Product Name</th>
											<th>Images</th>
											<th>Color</th>
											<th>Size</th>
											<th>Note</th>
											<th>Qty</th>
											<!-- <th>In Stock</th> -->
											<th>Kurang</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$no = 1;
											foreach($list_flag->result() as $row) :
												$qty = $row->qty;
												$total_book = $row->total_book;
												$stock = $row->stock;
												$kurang = (($stock) - $qty)*-1;
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td><b><a href="<?php echo site_url('PO/purchaseOrderDetail/') ?><?= $row->id_bon ?>"><?= $row->id_bon ?></a></b></td>
											<td><?= $row->po_group ?></td>
											<td><?= $row->name_costumer ?></td>
											<td><?= $row->name ?></td>
											<td><img border="1" src="<?= site_url('gambar/'.$row->images) ?>" width="100px"></td>
											<td><?= $row->color ?></td>
											<td><?= $row->size ?></td>
											<td><?= $row->note ?></td>
											<td><?= $row->qty ?></td>
											<!-- <td><?php echo $stock ?></td> -->
											<td><?php echo ($kurang < 0 ? 0 : $kurang) ?></td>
											<td><input type="text" name="<?= $row->id_bon ?>-<?= $row->id ?>-<?= $row->id_product ?>"></td>
										</tr>
										<?php
											endforeach;
										?>
									</tbody>
								</table>
							</div>
						</div><!-- end row -->
					</div><!-- container-fluid -->
					<div class="card-footer">
						<button class="btn btn-success pull-right" onclick="updateflag();">Update</button>
					</div>
				</div><!-- content -->
				<?php $this->load->view("_partials/footer.php") ?>
			</div><!-- ============================================================== -->
			<!-- End Right content here -->
			<!-- ============================================================== -->
		</div><!-- END wrapper -->
		<!-- jQuery  -->

		<?php $this->load->view("_partials/js.php") ?>
		<script type="text/javascript">
			$("#datatable").DataTable();

			function updateflag(){
				var table = $("#datatable")[0];
				for (var i = 1, row; row = table.rows[i]; i++) {
					var data = ((row.cells[11].children[0].value));
					var name = ((row.cells[11].children[0].getAttribute("name")));
					var qty = ((row.cells[9].innerHTML));
					var total_stock = ((row.cells[6].innerHTML));
					console.log(data + " " + name + " " + qty + " " + total_stock);
					console.log("<?php echo site_url('Flagging/flaggingupdate/') ?>"+name+"/"+data+"/"+qty);
					
					if(data != ""){
						$.ajax({
							url: "<?php echo site_url('Flagging/flaggingupdate/') ?>"+name+"/"+data+"/"+qty,
							async:false,
							success: function(data){
							}
						});
					}
					
				}
				window.location.reload(); 
			}

			function changegroup(id_group){
				if(id_group == 'All'){
					window.location.href = "<?php echo site_url('Flagging/flaggingListAll') ?>";
				}
				else{
					window.location.href = "<?php echo site_url('Flagging/flaggingListGroup/') ?>"+id_group;
				}
			}
		</script>

</body>

</html>
