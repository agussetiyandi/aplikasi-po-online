<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Update Purchase Order</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Update Purchase Order</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Purchase Order</a></li>
										<li class="breadcrumb-item active" aria-current="page">Update Purchase Order</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->

				<div class="row">
					<?php
						foreach($po_data->result() as $row) :
							$bank = $row->bank;
							$contact = $row->contact;
							$contact = explode(";",$contact)
					?>
					<div class="col-md-12">
						<div class="card">
							
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="p-20">
											<form method="POST" action="<?php echo site_url('PO/purchaseOrderUpdateData') ?>">
											<div class="form-group">
												<label>PO Batch</label>
												<select name="po_group" class="form-control">
													<?php foreach($list_po_group->result() as $group) : ?>
														<option value="<?= $group->id ?>" <?php echo ($group->id == $row->po_group ? "selected" : "") ?>><?= $group->id ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="p-20">
												<input name="id_bon" type="hidden" value="<?= $row->id_bon ?>">
												<div class="form-group">
													<label>Name</label>
													<input name="name" type="text" class="form-control" placeholder="Name" required value="<?= $row->name ?>">
												</div>
												<div class="form-group">
													<label>E-mail</label>
													<input name="email" type="email" class="form-control" placeholder="E-mail" value="<?= $row->email ?>">
												</div>
												<div class="form-group">
													<label>Phone</label>
													<input name="phone" type="text" class="form-control" placeholder="Phone" value="<?= $row->phone ?>">
												</div>
												<div class="form-group">
													<label>Contact</label>
													<select name="contact" class="form-control" onchange="contactCheck(this.value);">
														<option value="None" <?php echo ($contact[0]=="[None]" ? "selected" : "" ); ?>>None</option>
														<option value="Line @" <?php echo ($contact[0]=="[Line @]" ? "selected" : "" ); ?>>Line</option>
														<option value="BBM" <?php echo ($contact[0]=="[BBM]" ? "selected" : "" ); ?>>BBM</option>
														<option value="WhatsApp" <?php echo ($contact[0]=="[WhatsApp]" ? "selected" : "" ); ?>>WhatsApp</option>
														<option value="Instagram" <?php echo ($contact[0]=="[Instagram]" ? "selected" : "" ); ?>>Instagram</option>
													</select>
													<input name="idcontact" type="text" class="form-control" placeholder="ID Contact" value="<?php echo $contact[1] ?>"
													 <?php echo ($contact[0]=="None" ? "disabled" : "" ); ?>>
												</div>
										</div>
									</div>
									<div class="col-md-6">
										<form method="POST" action="<?php echo site_url('PO/purchaseOrderUpdateData') ?>">
											<div class="form-group">
												<label>Bank</label>
												<select name="bank" class="form-control">
													<option value="BCA">BCA</option>
													<option value="Mandiri">Mandiri</option>
												</select>
											</div>
											<div class="form-group">
												<label>Address</label>
												<textarea name="address" class="form-control" rows="5" placeholder=". . . . ."><?= $row->address ?></textarea>
											</div>
											<div class="form-group">
												<label>Note</label>
												<textarea name="note" class="form-control" rows="5" placeholder=". . . . ."><?= $row->note ?></textarea>
											</div>
											<div class="form-group mb-0">
												<div>
													<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<?php
						endforeach;
					?>
						</div><!-- end row -->

					</div><!-- container-fluid -->
				</div><!-- content -->
				<?php $this->load->view("_partials/footer.php") ?>
			</div><!-- ============================================================== -->
			<!-- End Right content here -->
			<!-- ============================================================== -->
		</div><!-- END wrapper -->
		<!-- jQuery  -->

		<?php $this->load->view("_partials/js.php") ?>
		<script type="text/javascript">
			function contactCheck(input) {
				if (input == 'None') {
					$("[name=idcontact]").prop("disabled", true);
				} else {
					$("[name=idcontact]").prop("disabled", false);
				}
			}

		</script>
</body>

</html>
