<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Product List</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Color Product No.<?= $id; ?></h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Product</a></li>
										<li class="breadcrumb-item" aria-current="page">List Product</li>
										<li class="breadcrumb-item active" aria-current="page">Color Product</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="card">
					<div class="card-header">
						<div class="pull-right">
							<a href="#" onclick="tambahcolor('<?= $id; ?>')" class="btn btn-success"><i class="fa fa-plus"></i> Add Color</a>
						</div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="10%">No.</th>
											<th>Color</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
											foreach($list_color->result() as $row) :
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $row->color ?></td>
											<td>
													<a title="Delete" href="#" onclick="deleteConfirm('<?= $id; ?>','<?= $row->color ?>');" type="button" class="btn btn-danger">
														<i class="fa fa-trash"></i> Delete
													</a>
											</td>
										</tr>
										<?php
											endforeach;
										?>
									</tbody>
								</table>
							</div>
						</div><!-- end row -->
					</div>
				</div>
			</div>
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<div id="deleteConfirmModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Delete Confirm</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<form action="<?php echo site_url('Product/colorDelete') ?>" method="post">
					<div class="modal-body">
							<h6>Are you sure to delete Color <span id='deleteProductId'>asd</span>??</h6>
							<input type="hidden" name="id_product" value="<?= $id; ?>">
							<input name="color" type="hidden" id="idcolor" class="form-control" value="" placeholder=". . ." required>
					</div>
					<div class="modal-footer">
						<button type="submit" name="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Delete</button>
						<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-times"></i> Close</button> 
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div id="colorModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Add Color</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<form action="<?php echo site_url('Product/addColor') ?>" method="post">
					<div class="modal-body">
						<div class="form-group">
							<label>Color</label>
							<input type="hidden" name="id_product" value="<?= $id; ?>">
							<input name="color" type="text" class="form-control" placeholder=". . ." required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" name="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> Save</button>
						<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-times"></i> Close</button> 
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>
  <script type="text/javascript">
		$("#datatable").DataTable();

	</script>

	<script type="text/javascript">
		$("#datatable").DataTable();
		function deleteConfirm(id_product,color) {
			console.log('buka modal');
			$("#deleteProductId").html(color);
			$("#idcolor").val(color);
			$("#linkdelete").prop("href", "<?php echo site_url('Product/colorDelete/') ?>"+color);
			$("#deleteConfirmModal").modal("show");
		}

		function tambahcolor(id_product) {
			console.log('buka modal');
			$("#colorId").html(id_product);
			$("#colorModal").modal("show");
		}
	</script>
</body>

</html>
