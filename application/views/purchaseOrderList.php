<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Purchase Order List</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left enlarged">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">List Purchase Order</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Purchase Order</a></li>
										<li class="breadcrumb-item active" aria-current="page">List Purchase Order</li>
									</ol>
								</div>
								<div class="col-md-4">
									<select class="form-control" onchange="changegroup(this.value);">
										<option value="All" <?php echo ($groupall == '1' ? 'selected' : '') ?>>All</option>
										<?php foreach($list_po_group->result() as $group) : ?>
										<option value="<?= $group->id ?>" <?php echo ($groupall == $group->id ? 'selected' : '') ?>><?= $group->id ?></option>
										<?php endforeach ;?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="card">
					<div class="card-body" style="overflow-x: auto">
						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">PO ID</th>
											<th width="1%">PO Group</th>
											<th>Status Payment</th>
											<th>Date</th>
											<th>Name</th>
											<th>Contact</th>
											<th>Notes</th>
											<th>Payment</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach($list_po->result() as $row) :
												$date = $row->date;
												$date = explode(" ", $date);

												$contact = $row->contact;
												$contact = explode(";", $contact);
												$current_payment = $row->current_payment;
												$total_payment = $row->total_payment;
										?>
										<tr>
											<td><?= $row->id_bon ?></td>
											<td><?= $row->po_group ?></td>
											<td><h5 class="m-0">
											<?php 
												if($total_payment == 0):
											?>
												<span class="badge badge-danger">Belum Order</span>
											<?php 
												elseif($current_payment < ($total_payment/2)):
											?>
												<span class="badge badge-danger">Belum Lunas</span>
											<?php
												elseif($current_payment < $total_payment):
											?>
												<span class="badge badge-warning">DP Lunas</span>
											<?php
												else:
											?>
												<span class="badge badge-success">Lunas</span>
											<?php
												endif;
											?>
											</h5></td>
											<td><?php echo implode("<br>", $date) ?></td>
											<td><?= $row->name ?><br><?= $row->email ?></td>
											<td><?= $row->phone ?><br><?php echo $contact[0]." ".$contact[1] ?></td>
											<td><?= $row->note ?></td>
											<td>
												<?php if($total_payment==0): ?>
													Empty
												<?php else: ?>
												<?php echo "Rp".number_format($current_payment)." of<br>Rp".number_format($total_payment) ?>
												<?php endif; ?>
											</td>
											<td>
												<div class="btn-group" style="color:white; border-radius: 5px; overflow: hidden;">
													<a href="<?php echo site_url('PO/purchaseOrderDetail/'.$row->id_bon) ?>" title="Add Details" type="button"
													 class="btn btn-info">
														<i class="fa fa-money"></i>
													</a>
													<a href="<?php echo site_url('PO/purchaseOrderUpdate/'.$row->id_bon) ?>" title="Edit" type="button" class="btn btn-warning">
														<i class="fa fa-pencil"></i>
													</a>
													
													<button onclick="deleteConfirm('<?= $row->id_bon ?>');" title="Delete" type="button" class="btn btn-danger">
														<i class="fa fa-trash"></i>
													</button>
												</div>
											</td>
										</tr>
										<?php
											endforeach;
										?>
									</tbody>
								</table>
							</div>
						</div><!-- end row -->
					</div>
				</div>
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div>

	<div id="deleteConfirmModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Delete Confirm</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<h6>Are you sure to delete PO.<b><span id='deletePOId'>asd</span></b></h6>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
					<a id="linkdelete" href="" class="btn btn-danger waves-effect waves-light">Delete</a>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>
	<script type="text/javascript">
		$("#datatable").DataTable();

		function deleteConfirm(id_bon) {
			console.log('buka modal');
			$("#deletePOId").html(id_bon);
			$("#linkdelete").prop("href", "<?php echo site_url('PO/purchaseOrderDelete/') ?>" + id_bon);
			$("#deleteConfirmModal").modal("show");
		}

		function changegroup(id_group){
				if(id_group == 'All'){
					window.location.href = "<?php echo site_url('PO/purchaseOrderListAll') ?>";
				}
				else{
					window.location.href = "<?php echo site_url('PO/purchaseOrderListGroup/') ?>"+id_group;
				}
			}
	</script>

</body>

</html>
