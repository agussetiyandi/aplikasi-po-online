<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Add Product</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Add Product</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Product</a></li>
										<li class="breadcrumb-item active" aria-current="page">Add Product</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-xl-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="p-20">
											<?php echo form_open_multipart('Product/addNewProduct'); ?>
											<div class="form-group">
												<label>Product Name</label>
												<input name="name" type="text" class="form-control" placeholder=". . ." required>
											</div>
											<div class="form-group">
												<label>Harga Modal [EUR]</label>
												<input name="modal_eur" type="text" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group">
												<label>Harga Modal [Rupiah]</label>
												<input name="ws_rp" type="text" class="form-control" placeholder=". . .">
											</div>
											<!-- <div class="form-group">
												<label class="control-label">Harga Modal EUR</label>
												<input type="text" name="modal_eur" class="form-control">
											</div>
											<div class="form-group">
												<label class="control-label">Harga WS Rupiah</label>
												<input type="text" name="ws_rp" class="form-control">
											</div> -->
											<div class="form-group">
												<label>Size</label>
												<input type="text" name="size" class="form-control" placeholder=". . .">
											</div>
											<!-- <div class="form-group">
												<label>Stock</label>
												<input type="number" name="stock" class="form-control" placeholder=". . ." value="0">
											</div> -->
											<!-- <div class="form-group">
												<label>Picture</label>
												<input type="file" name="picture" class="filestyle" data-buttonname="btn-secondary">
											</div> -->
										</div>
									</div>
									<div class="col-md-6">
										<div class="p-20">
											<?php echo form_open_multipart('Product/addNewProduct'); ?>
											<div class="form-group"><label>Product Category</label>
												<select name="category" class="form-control">
													<?php foreach($list_cat->result() as $row) : ?>
													<option value="<?= $row->product_category ?>">
														<?= $row->product_category ?>
													</option>
													<?php endforeach; ?>
												</select>
											</div>
											<!-- <div class="form-group">
												<label>Additional Fee</label>
												<input type="text" name="add_fee" class="form-control" placeholder=". . .">
											</div> -->
											<!-- <div class="form-group">
												<label class="control-label">Additional Fee</label>
												<input type="text" name="add_fee" class="form-control">
											</div>
											<div class="form-group">
												<label class="control-label">Harga Retail Rupiah</label>
												<input type="text" name="retail_rp" class="form-control">
											</div> -->
											<div class="form-group">
												<label>Color</label>
												<input type="text" name="color" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group"><label>Picture</label>
												<input type="file" name="picture" class="form-control">
											</div>
										</div>
									</div>
									<div class="form-group col-xl-6">
										<button type="submit" name="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
									</div>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end row -->
	</div><!-- container-fluid -->
	</div><!-- content -->
	<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>

</body>

</html>
