<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Product List</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Product</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Product</a></li>
										<li class="breadcrumb-item active" aria-current="page">List Product</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="card">
					<div class="card-body" style="overflow-x: auto">
						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="10%">No.</th>
											<th width="21%">Product Name</th>
											<th width="10%">Image</th>
											<th>Color</th>
											<th>Size</th>
											<th>Harga Modal</th>
											<!-- <th>Additional Fee</th> -->
											<!-- <th>Retail</th> -->
                      <th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
											foreach($list_product->result() as $row) :
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td>[<?= $row->product_category ?>] <?= $row->product_name ?></td>
											<td><img border="1" src="<?= site_url('gambar/'.$row->images) ?>" width="100px"></td>
											<!-- <td><?= $row->images ?></td> -->
											<td>
											<?php 
												foreach ($list_color->result() as $color):
													if($row->id_product == $color->id_product){
														echo $color->color."<br>";
													}
												endforeach;
											?>
											</td>
											<td>
											<?php 
												foreach ($list_size->result() as $size):
													if($row->id_product == $size->id_product){
														echo $size->size."<br>";
													}
												endforeach;
											?>
											</td>
											<td>EUR <?= $row->capital_price_usd ?> <br>Rp<?php echo number_format($row->price_rupiah) ?></td>
											<!-- <?= $row->additional_fee ?></td> -->
											<!-- <td>Rp<?php echo number_format($row->additional_fee) ?> </td> -->
											<td>
													<!-- <a title="Detail" href="#" type="button" class="btn btn-info">
														<i class="fa fa-eye"></i>
													</a> -->
													<a title="C" href="<?php echo site_url('Product/colorUpdate/'.$row->id_product) ?>" type="button" class="btn btn-success">
														<i class="fa fa-plus"></i> C
													</a>
													<a title="S" href="<?php echo site_url('Product/sizeUpdate/'.$row->id_product) ?>" type="button" class="btn btn-info">
														<i class="fa fa-plus"></i> S
													</a>
													<a title="Edit" href="<?php echo site_url('Product/productUpdate/'.$row->id_product) ?>" type="button" class="btn btn-warning">
														<i class="fa fa-pencil"></i> Edit
													</a>
													<a title="Delete" href="#" onclick="deleteConfirm('<?= $row->id_product ?>');"type="button" class="btn btn-danger">
														<i class="fa fa-trash"></i> Delete
													</a>
											</td>
										</tr>
										<?php
											endforeach;
										?>
									</tbody>
								</table>
							</div>
						</div><!-- end row -->
					</div>
				</div>
			</div>
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<div id="deleteConfirmModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Delete Confirm</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<h6>Are you sure to delete Product.<b><span id='deleteProductId'>asd</span></b></h6>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
					<a id="linkdelete" href="" class="btn btn-danger waves-effect waves-light">Delete</a>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>
  <script type="text/javascript">
		$("#datatable").DataTable();

	</script>

	<script type="text/javascript">
		$("#datatable").DataTable();
		function deleteConfirm(id_product) {
			console.log('buka modal');
			$("#deleteProductId").html(id_product);
			$("#linkdelete").prop("href", "<?php echo site_url('Product/productDelete/') ?>"+id_product);
			$("#deleteConfirmModal").modal("show");
		}
	</script>
</body>

</html>
