<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico"> <!--Morris Chart CSS -->
  
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morris/morris.css"> <!-- Basic Css files -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/metismenu.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/icons.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

<!-- DataTables -->
<link href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">