<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Details Purchase Order</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<?php
						foreach($po_data->result() as $row) :
							$total_payment = $row->total_payment;
							$current_payment = $row->current_payment;

					?>
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<!-- <a href="<?php echo site_url('PO/purchaseOrderList') ?>" title="Back" type="button" class="btn btn-info">
										<i class="fa fa-eye"></i>
									</a> -->
									<h4 class="page-title mb-0">BON #<?= $row->id_bon ?> [<?= $row->name ?>]</h4>
								</div>
							</div>
						</div>
					</div>
					<?php
						endforeach;
					?>
				</div><!-- end row -->

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="page-title mb-0 pull-left">Payment Information</h4>
							</div>
							<div class="card-body" style="overflow-x: auto">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th>Description</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Total Payment</td>
											<td>Rp<?php echo number_format($total_payment) ?></td>
											<!-- | DP 50% = Rp <?php echo number_format(($total_payment/2),2,",",".") ?> -->
										</tr>
										<tr>
											<td>Current Payment</td>
											<td>Rp<?php echo number_format($current_payment) ?></td>
										</tr>
										<tr>
											<td>Sisa Payment</td>
											<td>Rp<?php echo number_format(($total_payment-$current_payment)) ?></td>
										</tr>
										<tr>
											<td>Sisa Payment</td>
											<td><h5 class="m-0">
											<?php 
												if($total_payment == 0):
											?>
												<span class="badge badge-danger">Belum Order</span>
											<?php 
												elseif($current_payment < ($total_payment/2)):
											?>
												<span class="badge badge-danger">Belum Lunas</span>
											<?php
												elseif($current_payment < $total_payment):
											?>
												<span class="badge badge-warning">DP Lunas</span>
											<?php
												else:
											?>
												<span class="badge badge-success">Lunas</span>
											<?php
												endif;
											?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="page-title mb-0 pull-left">Product</h4>
								<button data-target="#AddProduct" data-toggle="modal" title="Add New Product" type="button" class="btn btn-success pull-right">
									<i class="fa fa-plus"></i>
								</button>
							</div>
							<div class="card-body" style="overflow-x: auto">
								<table id="datatable" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th>No.</th>
											<th>Product Name</th>
											<th>Color</th>
											<th>Size</th>
											<th>Qty</th>
											<th>Note</th>
											<th width="15%">Price</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php if(!isset($list_product) || count($list_product->result()) == 0): ?>
										<tr>
											<td colspan="7">No Data Available</td>
										</tr>
										<?php else: ?>
										<?php
											$i = 1;
											$total_price_product = 0;
											foreach ($list_product->result() as $product):
												$total_price_product += $product->price;
										?>
										<tr>
											<td><?= $i++ ?></td>
											<td><?= $product->name ?></td>
											<td><?= $product->color ?></td>
											<td><?= $product->size ?></td>
											<td><?= $product->qty ?></td>
											<td><?= $product->note ?></td>
											<td>Rp<?= number_format($product->price) ?></td>
											<td>
												<a href="<?php echo site_url('PO/purchaseOrderDetailDeleteProduct/'.$product->id.'/'.$product->id_bon.'/'.$product->price) ?>" onclick="return confirm('Are you sure?')" title="Delete" type="button" class="btn btn-danger">
													<i class="fa fa-times"></i>
												</a>
											</td>
										</tr>
										<?php
											endforeach;
										?>
										<tr>
											<td colspan="6">Total</td>
											<td>Rp<?php echo number_format($total_price_product) ?></td>
										</tr>
										<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="page-title mb-0 pull-left">Etc</h4>
								<button data-target="#AddProductEtc" data-toggle="modal" title="Add New Etc" type="button" class="btn btn-success pull-right">
									<i class="fa fa-plus"></i>
								</button>
							</div>
							<div class="card-body" style="overflow-x: auto">
								<table id="datatable2" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th>Name</th>
											<th>Notes</th>
											<th>Qty</th>
											<th>Price</th>
											<th>Total</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php if(!isset($list_product_etc) || count($list_product_etc->result()) == 0): ?>
										<tr>
											<td colspan="6">No Data Available</td>
										</tr>
										<?php else: ?>
										<?php
											$total_price_product_etc = 0;
											foreach ($list_product_etc->result() as $etc):
												$qty = $etc->quantity;
												$price = $etc->price;
												$total = $qty*$price;
												$total_price_product_etc += $total;
										?>
										<tr>
											<td><?= $etc->name ?></td>
											<td><?= $etc->note ?></td>
											<td><?= $etc->quantity ?></td>
											<td><?= $etc->price ?></td>
											<td>Rp<?php echo number_format($total); ?></td>
											<td>
												<a href="<?php echo site_url('PO/purchaseOrderDetailDeleteProductEtc/'.$etc->id.'/'.$etc->id_bon.'/'.$total) ?>" onclick="return confirm('Are you sure?')" title="Delete" type="button" class="btn btn-danger">
													<i class="fa fa-times"></i>
												</a>
											</td>
										</tr>
										<?php
											endforeach;
										?>
										<tr>
											<td colspan="4">Total</td>
											<td>Rp<?php echo number_format($total_price_product_etc) ?></td>
										</tr>
										<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="page-title mb-0 pull-left">Payment</h4>
								<button data-target="#AddPayment" data-toggle="modal" title="Add New Payment" type="button" class="btn btn-success pull-right">
									<i class="fa fa-plus"></i>
								</button>
							</div>
							<div class="card-body" style="overflow-x: auto">
								<table id="datatable3" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width='1%'>No.</th>
											<th>Payment</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php if(!isset($list_payment) || count($list_payment->result()) == 0): ?>
										<tr>
											<td colspan="3">No Data Available</td>
										</tr>
										<?php else: ?>
										
										<?php
										$i=1;
											foreach ($list_payment->result() as $payment):
										?>
										<tr>
											<td width='1%'><?= $i++ ?></td>
											<td>Rp<?= number_format($payment->payment) ?></td>
											<!-- <td><?= $payment->payment ?></td> -->
											<td>
												<a href="<?php echo site_url('PO/purchaseOrderDetailDeletePayment/'.$payment->id.'/'.$payment->id_bon.'/'.$payment->payment) ?>" onclick="return confirm('Are you sure?')" title="Delete" type="button" class="btn btn-danger">
													<i class="fa fa-times"></i>
												</a>
											</td>
										</tr>
										<?php
											endforeach;
										?>
										<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="page-title mb-0 pull-left">Resi Pengiriman</h4>
								<button data-target="#AddResi" data-toggle="modal" title="Add New Resi" type="button" class="btn btn-success pull-right">
									<i class="fa fa-plus"></i>
								</button>
							</div>
							<div class="card-body" style="overflow-x: auto">
								<table id="datatable4" class="table table-bordered dt-responsive nowrap text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width='1%'>No.</th>
											<th>Ekspedisi</th>
											<th>No. Resi</th>
										</tr>
									</thead>
									<tbody>
										<?php if(!isset($list_resi) || count($list_resi->result()) == 0): ?>
										<tr>
											<td colspan="3">No Data Available</td>
										</tr>
										<?php else: ?>
										<?php
										$i=1;
											foreach ($list_resi->result() as $resi):
										?>
										<tr>
											<td width='1%'><?= $i++ ?></td>
											<td><?= $resi->sender ?></td>
											<td><?= $resi->resi ?></td>
										</tr>
										<?php
											endforeach;
										?>
										<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->

			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div>

	<div id="AddProduct" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Add New Product</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<form method="POST" action="<?php echo site_url('PO/purchaseOrderDetailAddProduct') ?>">
					<div class="modal-body">
						<div class="form-group">
							<label>Product</label> 
							<select onchange="dinamikform(this.value);" name="product" class="form-control" required>
								<option value="">- Pilih Product -</option>
								<?php
									$i=1;
										foreach ($list_product_select->result() as $select):
									?>
									<option value="<?= $select->id_product ?>"><?= $select->product_name ?></option>
									<?php
										endforeach;
								?>
							</select>
						</div>
						<span id="dynamicform"></span>
						<input name="id_bon" type="hidden" class="form-control" placeholder="Phone" required value="<?= $row->id_bon ?>">
					</div>
					<div class="modal-footer">
					<div class="form-group mb-0">
						<div>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
							<button type="submit" class="btn btn-success waves-effect"><i class="fa fa-plus"></i> Add</button> 
						</div>
					</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div id="AddProductEtc" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Add New Product Etc</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<form method="POST" action="<?php echo site_url('PO/purchaseOrderDetailAddProductEtc') ?>">
					<div class="modal-body">
						<div class="form-group">
							<label>Name</label> 
							<input name="name" type="text" class="form-control" placeholder="Name of Product" required>
						</div>
						<div class="form-group">
							<label>Quantity</label> 
							<input name="quantity" type="number" class="form-control" placeholder="Quantity" required>
						</div>
						<div class="form-group">
							<label>Price / Quantity</label> 
							<input name="price" type="number" class="form-control" placeholder="Price" required>
						</div>
						<div class="form-group">
							<label>Note</label> 
							<textarea name="note" class="form-control" rows="5"></textarea>
						</div>
						<input name="id_bon" type="hidden" class="form-control" placeholder="Phone" required value="<?= $row->id_bon ?>">
					</div>
					<div class="modal-footer">
					<div class="form-group mb-0">
						<div>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
							<button type="submit" class="btn btn-success waves-effect"><i class="fa fa-plus"></i> Add</button> 
						</div>
					</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div id="AddPayment" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Add New Payment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<form method="POST" action="<?php echo site_url('PO/purchaseOrderDetailAddPayment') ?>">
					<div class="modal-body">
						<div class="form-group">
							<label>Payment</label> 
							<input name="payment" type="number" class="form-control" placeholder=". . ." required>
						</div>
						<input name="id_bon" type="hidden" class="form-control" placeholder="Phone" required value="<?= $row->id_bon ?>">
					</div>
					<div class="modal-footer">
					<div class="form-group mb-0">
						<div>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
							<button type="submit" class="btn btn-success waves-effect"><i class="fa fa-plus"></i> Add</button> 
						</div>
					</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div id="AddResi" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title mt-0" id="myModalLabel">Add New Resi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<form method="POST" action="<?php echo site_url('PO/purchaseOrderDetailAddResi') ?>">
					<div class="modal-body">
						<div class="form-group">
							<label>Ekspedisi</label> 
							<select name="sender" class="form-control">
								<option value="TIKI">TIKI</option>
								<option value="JNE">JNE</option>
								<option value="GOJEK">GOJEK</option>
							</select>
						</div>
						<div class="form-group">
							<label>No. Resi</label> 
							<input name="resi" type="text" class="form-control" placeholder="Resi" required>
						</div>
						<input name="id_bon" type="hidden" class="form-control" placeholder="Phone" required value="<?= $row->id_bon ?>">
					</div>
					<div class="modal-footer">
					<div class="form-group mb-0">
						<div>
							<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button> 
							<button type="submit" class="btn btn-success waves-effect"><i class="fa fa-plus"></i> Add</button> 
						</div>
					</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>
	<script type="text/javascript">
		// $("#datatable, #datatable2, #datatable3, #datatable4").DataTable({
		// 	"lengthChange":   false,
		// 	"searching":   false,
		// 	"sort": false
		// });
		function dinamikform(product){
			console.log(product);
			if(product==""){
				$("#dynamicform").html("");
			}
			else{
				$.ajax({
					url: "<?php echo site_url('PO/purchaseOrderDynamicForm/') ?>"+product,
					success: function(data){
						console.log(data);
						$("#dynamicform").html(data);
					}
				}).done(function(data){
					console.log(data);
					$("#dynamicform").html(data);
				});
			}
			
		}
	</script>
</body>

</html>
