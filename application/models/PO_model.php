<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PO_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_po()
	{
		$this->db->select("*");
		$this->db->from("purchase_order");

		return $this->db->get();
	}

	function get_last_po()
	{
		$this->db->select("MAX(ID) as id");
		$this->db->from("purchase_order_group");
		return $this->db->get();
	}

	function get_all_po_pogroup($po_group)
	{
		$this->db->where("po_group", $po_group);
		$this->db->select("*");
		$this->db->from("purchase_order");

		return $this->db->get();
	}

	function get_po($id_bon)
	{
		$this->db->where("id_bon", $id_bon);
		$this->db->select("*");
		$this->db->from("purchase_order");

		return $this->db->get();
	}


	function purchaseOrderInsertDB($data){
		$this->db->insert("purchase_order", $data);
	}

	function purchaseOrderUpdateDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("purchase_order", $data);
	}

	function purchaseOrderDeleteDB($data){
		$this->db->where("id_bon", $data);
		$this->db->delete("purchase_order");
	}

	function purchaseOrderDetailAddResiDB($data){
		$this->db->insert("purchase_order_resi", $data);
	}

	function get_all_resi($id_bon)
	{
		$this->db->where("id_bon", $id_bon);
		$this->db->select("*");
		$this->db->from("purchase_order_resi");

		return $this->db->get();
	}

	function purchaseOrderDetailAddPaymentDB($data, $id_bon, $qty){
		$this->db->insert("purchase_order_payment", $data);

		$this->db->set('current_payment', '`current_payment`+'.$qty, FALSE);
		$this->db->where('id_bon', $id_bon);
		$this->db->update("purchase_order");
	}

	function purchaseOrderDetailDeletePaymentDB($data, $id_bon, $qty){
		$this->db->where("id", $data);
		$this->db->delete("purchase_order_payment");

		$this->db->set('current_payment', '`current_payment`-'.$qty, FALSE);
		$this->db->where('id_bon', $id_bon);
		$this->db->update("purchase_order");
	}

	function get_all_payment($id_bon)
	{
		$this->db->where("id_bon", $id_bon);
		$this->db->select("*");
		$this->db->from("purchase_order_payment");

		return $this->db->get();
	}

	function purchaseOrderDetailAddProductEtcDB($data, $id_bon, $qty){
		$this->db->insert("purchase_order_product_etc", $data);

		$this->db->set('total_payment', '`total_payment`+'.$qty, FALSE);
		$this->db->where('id_bon', $id_bon);
		$this->db->update("purchase_order");
	}

	function purchaseOrderDetailDeleteProductEtcDB($data, $id_bon, $qty){
		$this->db->where("id", $data);
		$this->db->delete("purchase_order_product_etc");

		$this->db->set('total_payment', '`total_payment`-'.$qty, FALSE);
		$this->db->where('id_bon', $id_bon);
		$this->db->update("purchase_order");
	}

	function get_all_product_etc($id_bon)
	{
		$this->db->where("id_bon", $id_bon);
		$this->db->select("*");
		$this->db->from("purchase_order_product_etc");

		return $this->db->get();
	}

	function purchaseOrderDetailAddProductDB($data, $id_bon, $qty){
		$this->db->insert("purchase_order_product", $data);

		$this->db->set('total_payment', '`total_payment`+'.$qty, FALSE);
		$this->db->where('id_bon', $id_bon);
		$this->db->update("purchase_order");
	}

	function purchaseOrderDetailDeleteProductDB($data, $id_bon, $qty){
		$this->db->where("id", $data);
		$this->db->delete("purchase_order_product");

		$this->db->set('total_payment', '`total_payment`-'.$qty, FALSE);
		$this->db->where('id_bon', $id_bon);
		$this->db->update("purchase_order");
	}

	function get_all_product($id_bon)
	{
		$this->db->where("id_bon", $id_bon);
		$this->db->select("*");
		$this->db->from("purchase_order_product");

		return $this->db->get();
	}

	function catInsertDB($data){
		$this->db->insert("purchase_order_batch", $data);
	}

	function catDeleteDB($data){
		$this->db->where("id_po_batch", $data);
		$this->db->delete("po_batch");
	}

}
