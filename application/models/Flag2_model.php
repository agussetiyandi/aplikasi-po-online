<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flag2_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_flag()
	{
		$this->db->select("*");
		$this->db->from("v_flagging2");

		return $this->db->get();
	}

	function get_all_flag_last_po()
	{
		// $sql = "SELECT * FROM v_flagging2";
		// $this->db->query($sql);
		$this->db->where("`po_group` IN (SELECT MAX(ID) FROM purchase_order_group)");
		$this->db->select("*");
		$this->db->from("v_flagging2");
		return $this->db->get();
	}

	function get_last_po()
	{
		$this->db->select("MAX(ID) as id");
		$this->db->from("purchase_order_group");
		return $this->db->get();
	}

	function get_all_flag_pogroup($po_group)
	{
		$this->db->where("po_group", $po_group);
		$this->db->select("*");
		$this->db->from("v_flagging2");

		return $this->db->get();
	}

	function get_flag($id)
	{
		$this->db->where("id", $id);
		$this->db->select("*");
		$this->db->from("v_flagging2");

		return $this->db->get();
	}

	function flagging2CancelDB($data, $condition){
		$sql = "UPDATE product AS a JOIN purchase_order_product AS b ON a.`id_product` = b.`id_product` SET a.stock = a.stock+b.flag2_qty-b.action_qty WHERE b.id = ".$condition['id']."";
		$this->db->query($sql);

		$this->db->where($condition);
		$this->db->update("purchase_order_product", $data);
	}

	function flagging2SendDB($data, $condition, $condition2, $qty){
		$this->db->where($condition);
		$this->db->update("purchase_order_product", $data);

		$this->db->set('stock', '`stock`-'.$qty, FALSE);
		$this->db->where($condition2);
		$this->db->update("product");
	}

	function flagging2UpdateQtyDB($data, $condition){
		// $this->db->where($condition);
		// $this->db->update("purchase_order_product", $data);

		$this->db->set('flag2_qty', '`flag2_qty`+'.$data, FALSE);
		$this->db->where($condition);
		$this->db->update("purchase_order_product");
	}
}
