<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flag_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_flag()
	{
		// $this->db->where("(total_payment) / 2<=current_payment");
		// $this->db->where("flag <>", 2);
		// $this->db->select("purchase_order.id_bon, purchase_order_product.id_product, purchase_order_product.name, purchase_order_product.size, purchase_order_product.color, purchase_order_product.qty");
		// $this->db->from("purchase_order");
		// $this->db->join('purchase_order_product', 'purchase_order.id_bon=purchase_order_product.id_bon', 'inner');

		$this->db->select("*");
		$this->db->from("v_flagging");

		return $this->db->get();
	}

	function get_last_po()
	{
		$this->db->select("MAX(ID) as id");
		$this->db->from("purchase_order_group");
		return $this->db->get();
	}
	
	function get_all_flag_pogroup($po_group)
	{
		$this->db->where("po_group", $po_group);
		$this->db->select("*");
		$this->db->from("v_flagging");

		return $this->db->get();
	}

	function flaggingUpdateDB($id, $qty, $date){
		// $this->db->where($condition);
		// $this->db->update("purchase_order_product", $data);

		// $this->db->set('stock', '`stock`+'.$qty, FALSE);
		// $this->db->where($condition2);
		// $this->db->update("product");

		$sql = "UPDATE product AS a JOIN purchase_order_product AS b ON a.`id_product` = b.`id_product` SET a.stock = CASE WHEN (a.stock+".$qty.") < b.`qty` THEN 0 ELSE (a.stock-b.qty+".$qty.") END, b.`flag2_qty` =  CASE WHEN (a.stock+".$qty.") < b.`qty` THEN a.stock+".$qty." ELSE b.`qty` END, b.`flag2_date` ='".$date."', b.`flag` = 2, b.`action_qty` = ".$qty." WHERE b.id = ".$id."";
		$this->db->query($sql);

		
	}
}
