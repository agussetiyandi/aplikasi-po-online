<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class D_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select("name, count(id_product) as item ,sum(current_payment) as total");
		$this->db->from("purchase_order");

		return $this->db->get();
	}

}
