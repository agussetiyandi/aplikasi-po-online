<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_po_group()
	{
		$this->db->select("*");
		$this->db->from("purchase_order_group");
		$this->db->order_by("date_created", "desc");


		return $this->db->get();
	}

	function get_num_po_group()
	{
		$this->db->select("*");
		$this->db->from("purchase_order_group");
		$this->db->order_by("date_created", "desc");


		return $this->db->count_all_results();
	}

	function get_po_group($id)
	{
		$this->db->where("id", $id);
		$this->db->select("*");
		$this->db->from("purchase_order_group");

		return $this->db->get();
	}

	function settingAddPOGroupDB($data){
		$this->db->insert("purchase_order_group", $data);
	}

	function productInsertSize($data){
		$this->db->insert("product_size", $data);
	}

	function productInsertColor($data){
		$this->db->insert("product_color", $data);
	}

	function productUpdateDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("product", $data);
	}

	function productDeleteDB($data){
		$this->db->where("id_product", $data);
		$this->db->delete("product");
	}

	function get_all_cat()
	{
		$this->db->select("*");
		$this->db->from("product_category");

		return $this->db->get();
	}

	function get_cat($id_cat)
	{
		$this->db->where("id_category", $id_cat);
		$this->db->select("*");
		$this->db->from("product_category");

		return $this->db->get();
	}

	function catInsertDB($data){
		$this->db->insert("product_category", $data);
	}

	function catDeleteDB($data){
		$this->db->where("id_category", $data);
		$this->db->delete("product_category");
	}

	function catDeletePoGroup($data){
		$this->db->where("id", $data);
		$this->db->delete("purchase_order_group");
	}
}
