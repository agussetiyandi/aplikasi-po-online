<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select("*");
		$this->db->from("product");

		return $this->db->get();
	}

	function get_color()
	{
		$this->db->select("*");
		$this->db->from("product_color");

		return $this->db->get();
	}

	function get_size()
	{
		$this->db->select("*");
		$this->db->from("product_size");

		return $this->db->get();
	}

	//hoho
	function get_color_one($id_product)
	{
		$this->db->where("id_product", $id_product);
		$this->db->select("*");
		$this->db->from("product_color");

		return $this->db->get();
	}

	function get_size_one($id_product)
	{
		$this->db->where("id_product", $id_product);
		$this->db->select("*");
		$this->db->from("product_size");

		return $this->db->get();
	}
	//hihi

	function get_product($id_product)
	{
		$this->db->where("id_product", $id_product);
		$this->db->select("*");
		$this->db->from("product");

		return $this->db->get();
	}

	function productInsertDB($data){
		$this->db->insert("product", $data);
		return $id = $this->db->insert_id();
	}

	function productInsertSize($data){
		$this->db->insert("product_size", $data);
	}

	function productInsertColor($data){
		$this->db->insert("product_color", $data);
	}

	function productUpdateDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("product", $data);
	}

	function productDeleteDB($data){
		$this->db->where("id_product", $data);
		$this->db->delete("product");
	}

	function productDeleteColor($data){
		$this->db->where($data);
		$this->db->delete("product_color");
	}

	function productDeleteSize($data){
		$this->db->where($data);
		$this->db->delete("product_size");
	}

	function get_all_cat()
	{
		$this->db->select("*");
		$this->db->from("product_category");

		return $this->db->get();
	}

	function get_cat($id_cat)
	{
		$this->db->where("id_category", $id_cat);
		$this->db->select("*");
		$this->db->from("product_category");

		return $this->db->get();
	}

	function catInsertDB($data){
		$this->db->insert("product_category", $data);
	}

	function catDeleteDB($data){
		$this->db->where("id_category", $data);
		$this->db->delete("product_category");
	}
}
