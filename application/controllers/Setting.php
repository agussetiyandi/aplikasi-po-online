<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Setting_model"); 
	}

	function setting()
	{
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['num_po_group'] = $this->Setting_model->get_num_po_group();
		$this->load->view('setting', $data);
	}

	function settingAddPOGroup()
	{
		$data = array(
			'id' => $this->input->post('id'),
			'note' => $this->input->post('note'),
			'kurs' => $this->input->post('kurs')
		);
		$this->Setting_model->settingAddPOGroupDB($data);
		redirect('Setting/setting');
	}

	function catDeletePoGroup($data){
		$this->Setting_model->catDeletePoGroup($data);
		redirect('Setting/setting');
	}
}