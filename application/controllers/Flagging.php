<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flagging extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Flag_model"); 
		$this->load->model("Setting_model"); 
	}

	function flaggingList()
	{
		// $data['list_flag'] = $this->Flag_model->get_all_flag();
		// $this->load->view('flaggingList', $data);

		$lastpo = $this->Flag_model->get_last_po();
		$lastpo = $lastpo->result();
		$po_group = $lastpo[0]->id;
		$data['list_flag'] = $this->Flag_model->get_all_flag_pogroup($po_group);
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = $lastpo[0]->id;
		$this->load->view('flaggingList', $data);
	}

	function flaggingListGroup($po_group)
	{
		$data['list_flag'] = $this->Flag_model->get_all_flag_pogroup($po_group);
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = $po_group;
		$this->load->view('flaggingList', $data);
	}

	function flaggingListAll()
	{
		$data['list_flag'] = $this->Flag_model->get_all_flag();
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = '1';
		$this->load->view('flaggingList', $data);
	}

	function flaggingupdate($name, $input, $qty)
	{
		// $stock = $stock + $input;
		// $flag2_qty = $qty;
		// if($qty > $stock){
		// 	$flag2_qty = $stock;
		// }

		// $data = array(
		// 	'flag' => 2,
		// 	'flag2_qty' => $flag2_qty,
		// 	'flag2_date' => date('Y-m-d H:i:s')
		// );

		$id = explode("-",$name);
		// $condition['id_bon'] = $id[0];
		// $condition['id'] = $id[1];

		// $data2 = $input;
		// $condition2['id_product'] = $id[2];

		// $this->Flag_model->flaggingUpdateDB($data, $condition, $data2, $condition2);
		$this->Flag_model->flaggingUpdateDB($id[1], $input, date('Y-m-d H:i:s'));
	}
}