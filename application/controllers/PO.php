<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PO extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("PO_model"); 
		$this->load->model("Product_model"); 
		$this->load->model("Setting_model"); 
	}

	function purchaseOrderList()
	{
		// $data['list_po'] = $this->PO_model->get_all_po();
		// $this->load->view('purchaseOrderList', $data);

		$lastpo = $this->PO_model->get_last_po();
		$lastpo = $lastpo->result();
		$po_group = $lastpo[0]->id;
		$data['list_po'] = $this->PO_model->get_all_po_pogroup($po_group);
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = $lastpo[0]->id;
		$this->load->view('purchaseOrderList', $data);
	}

	function purchaseOrderListGroup($po_group)
	{
		$data['list_po'] = $this->PO_model->get_all_po_pogroup($po_group);
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = $po_group;
		$this->load->view('purchaseOrderList', $data);
	}

	function purchaseOrderListAll()
	{
		$data['list_po'] = $this->PO_model->get_all_po();
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = '1';
		$this->load->view('purchaseOrderList', $data);
	}

	function purchaseOrderCreate()
	{
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$this->load->view('purchaseOrderCreate', $data);
	}

	function purchaseOrderInsert(){
		$contact = "[";
		$contact .= $this->input->post('contact');
		$contact .= "];";
		$contact .= $this->input->post('idcontact');
		$data = array(
			'name' => $this->input->post('name'),
			'po_group' => $this->input->post('po_group'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'contact' => $contact,
			'bank' => $this->input->post('bank'),
			'address' => $this->input->post('address'),
			'note' => $this->input->post('note')
		);
		$this->PO_model->purchaseOrderInsertDB($data);
		redirect('PO/purchaseOrderList');
	}

	function purchaseOrderUpdate($id_bon)
	{
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['po_data'] = $this->PO_model->get_po($id_bon);
		$this->load->view('purchaseOrderUpdate', $data);
	}

	function purchaseOrderUpdateData(){
		$contact = "[";
		$contact .= $this->input->post('contact');
		$contact .= "];";
		$contact .= $this->input->post('idcontact');
		$data = array(
			'name' => $this->input->post('name'),
			'po_group' => $this->input->post('po_group'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'contact' => $contact,
			'bank' => $this->input->post('bank'),
			'address' => $this->input->post('address'),
			'note' => $this->input->post('note')
		);
		$condition['id_bon'] = $this->input->post('id_bon');
		$this->PO_model->purchaseOrderUpdateDB($data, $condition);
		redirect('PO/purchaseOrderList');
	}

	function purchaseOrderDelete($data){
		$this->PO_model->purchaseOrderDeleteDB($data);
		redirect('PO/purchaseOrderList');
	}

	function purchaseOrderDetail($id_bon)
	{
		$data['po_data'] = $this->PO_model->get_po($id_bon);
		$data['list_product_etc'] = $this->PO_model->get_all_product_etc($id_bon);
		$data['list_product'] = $this->PO_model->get_all_product($id_bon);
		$data['list_payment'] = $this->PO_model->get_all_payment($id_bon);
		$data['list_resi'] = $this->PO_model->get_all_resi($id_bon);
		$data['list_product_select'] = $this->Product_model->get_all();
		$this->load->view('purchaseOrderDetail', $data);
	}

	function purchaseOrderDetailAddResi(){
		$data2 = array(
			'id_bon' => $this->input->post('id_bon'),
			'sender' => $this->input->post('sender'),
			'resi' => $this->input->post('resi')
		);
		$this->PO_model->purchaseOrderDetailAddResiDB($data2);
		$id_bon = $this->input->post('id_bon');
		$data['po_data'] = $this->PO_model->get_po($id_bon);
		redirect('PO/purchaseOrderDetail/'.$id_bon);
	}

	function purchaseOrderDetailAddPayment(){
		$data2 = array(
			'id_bon' => $this->input->post('id_bon'),
			'payment' => $this->input->post('payment')
		);
		$id_bon = $this->input->post('id_bon');
		$payment = $this->input->post('payment');
		$this->PO_model->purchaseOrderDetailAddPaymentDB($data2, $id_bon, $payment);
		$data['po_data'] = $this->PO_model->get_po($id_bon);
		redirect('PO/purchaseOrderDetail/'.$id_bon);
	}
	function purchaseOrderDetailDeletePayment($data, $id_bon, $qty){
		$this->PO_model->purchaseOrderDetailDeletePaymentDB($data, $id_bon, $qty);
		redirect('PO/purchaseOrderDetail/'.$id_bon);
	}

	function purchaseOrderDetailAddProductEtc(){
		$data2 = array(
			'id_bon' => $this->input->post('id_bon'),
			'name' => $this->input->post('name'),
			'quantity' => $this->input->post('quantity'),
			'price' => $this->input->post('price'),
			'note' => $this->input->post('note')
		);
		$id_bon = $this->input->post('id_bon');
		$quantity = $this->input->post('quantity');
		$price = $this->input->post('price');
		$payment = $quantity * $price;
		$this->PO_model->purchaseOrderDetailAddProductEtcDB($data2, $id_bon, $payment);
		$data['po_data'] = $this->PO_model->get_po($id_bon);
		redirect('PO/purchaseOrderDetail/'.$id_bon);
	}

	function purchaseOrderDetailDeleteProductEtc($data, $id_bon, $qty){
		$this->PO_model->purchaseOrderDetailDeleteProductEtcDB($data, $id_bon, $qty);
		redirect('PO/purchaseOrderDetail/'.$id_bon);
	}

	function purchaseOrderDynamicForm($id_product)
	{
		$data['list_color'] = $this->Product_model->get_color_one($id_product); 
		$data['list_size'] = $this->Product_model->get_size_one($id_product); 
		$data['list_product'] = $this->Product_model->get_product($id_product); 
		$this->load->view('purchaseOrderDynamicForm', $data);
	}

	function purchaseOrderDetailAddProduct(){
		$data2 = array(
			'id_bon' => $this->input->post('id_bon'),
			'id_product' => $this->input->post('product'),
			'name' => $this->input->post('name'),
			'qty' => $this->input->post('quantity'),
			'color' => $this->input->post('color'),
			'size' => $this->input->post('size'),
			'price' => $this->input->post('price'),
			'note' => $this->input->post('note')
		);
		$id_bon = $this->input->post('id_bon');
		$price = $this->input->post('price');
		$this->PO_model->purchaseOrderDetailAddProductDB($data2, $id_bon, $price);
		$data['po_data'] = $this->PO_model->get_po($id_bon);
		redirect('PO/purchaseOrderDetail/'.$id_bon);
	}

	function purchaseOrderDetailDeleteProduct($data, $id_bon, $qty){
		$this->PO_model->purchaseOrderDetailDeleteProductDB($data, $id_bon, $qty);
		redirect('PO/purchaseOrderDetail/'.$id_bon);
	}

	function purchaseOrderBatch()
	{
		// $data['list_cat'] = $this->PO->get_all_cat(); 
		$this->load->view('purchaseOrderBatch', $data);
	}
	
	function catInsert(){
		$data = array(
			'purchase_order_batch' => $this->input->post('category')
		);
		$this->PO->catInsertDB($data);
		redirect('PO/purchaseOrderBatch');
	}

	function catDelete($data){
		$this->PO->catDeleteDB($data);
		redirect('PO/purchaseOrderBatch');
	}

}