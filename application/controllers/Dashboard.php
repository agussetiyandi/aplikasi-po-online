<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("PO_model");
	}

	public function index()
	{
		$data['list_po'] = $this->PO_model->get_all_po();
		$this->load->view('dashboard',$data);
	}
}
