<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Pdf');
		$this->load->model('PO_model');
	}

	public function index(){
		$this->load->view('printer');
	}

	function printBon(){
		$id = $this->input->post('po_id');
		$data = array(
			'data' => $this->PO_model->get_po($id),
			'list_product' => $this->PO_model->get_all_product($id)
		);
		$cek = $this->PO_model->get_po($id)->num_rows();
		if($cek > 0){
			$this->load->view('print', $data);
		}else{
			echo $this->session->set_flashdata('msg','ID PO Tidak Ditemukan, Silahkan Coba Lagi!');
			redirect('Printer');
		}
	}
}
