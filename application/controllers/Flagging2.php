<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flagging2 extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Flag2_model"); 
		$this->load->model("Product_model"); 
		$this->load->model("Setting_model"); 
	}

	function flagging2List()
	{
		// $data['list_flag'] = $this->Flag2_model->get_all_flag_last_po();
		// $data['list_po_group'] = $this->Setting_model->get_all_po_group();

		$lastpo = $this->Flag2_model->get_last_po();
		$lastpo = $lastpo->result();
		$po_group = $lastpo[0]->id;
		$data['list_flag'] = $this->Flag2_model->get_all_flag_pogroup($po_group);
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = $lastpo[0]->id;
		$this->load->view('flagging2List', $data);
	}

	function flagging2ListGroup($po_group)
	{
		$data['list_flag'] = $this->Flag2_model->get_all_flag_pogroup($po_group);
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = $po_group;
		$this->load->view('flagging2List', $data);
	}

	function flagging2ListAll()
	{
		$data['list_flag'] = $this->Flag2_model->get_all_flag();
		$data['list_po_group'] = $this->Setting_model->get_all_po_group();
		$data['groupall'] = '1';
		$this->load->view('flagging2List', $data);
	}

	function flagging2Cancel($id)
	{
		$data = array(
			'flag' => 1,
			'flag2_qty' => NULL,
			'flag2_date' => NULL
		);
		$condition['id'] = $id;

		$this->Flag2_model->flagging2CancelDB($data, $condition);
		redirect('Flagging2/flagging2List');
	}

	function flagging2Send($id, $id_product, $qty)
	{
		$data = array(
			'flag' => 3,
			'flag2_date' => date('Y-m-d H:i:s')
		);
		$condition['id'] = $id;

		$condition2['id_product'] = $id_product;

		$this->Flag2_model->flagging2SendDB($data, $condition, $condition2, $qty);
		redirect('Flagging2/flagging2List');
	}

	function flagging2UpdateQtyModal($id_product, $id, $link)
	{
		$data = array(
    		'product_data' => $this->Product_model->get_product($id_product),
    		'flag_data' => $this->Flag2_model->get_flag($id)
    	);
		$data['link'] = $link;
		$this->load->view('flagging2UpdateQtyModal', $data);
	}

	function flagging2UpdateQty(){
		// $data = array(
		// 	'flag2_qty' => $this->input->post('total_order')
		// );

		$condition['id'] = $this->input->post('id');
		$data = $this->input->post('total_order');
		$link = $this->input->post('link');
		$this->Flag2_model->flagging2UpdateQtyDB($data, $condition);
		if($link == 1){
			redirect('Flagging2/flagging2ListAll');
		}
		else{
			redirect('Flagging2/flagging2ListGroup/'.$link);
		}
	}

}